const catalogList = document.querySelector('#catalog-list .body ul');

const formElements = {
    imgSource           : document.querySelector('#img-source'),
    productName         : document.querySelector('#product-name'),
    productPrice        : document.querySelector('#product-price'),
    productDescription  : document.querySelector('#product-description'),
    submitNewProduct    : document.querySelector('#add-new-product')
};
 
// product object creator
function Product(imgSource, name, price, description){
    this.imgSource = imgSource;
    this.name = name;
    this.price = price;
    this.description = description;
}


let removeBtns = document.querySelectorAll('li .remove');

const productManager = {
    // constructor for catalog li item 
    addProudct: (product) => { 
        let listItem =  '<li class="product"> <img src="' + product.imgSource + '">';
        listItem    +=  '<h4 class="name">' + product.name + '</h4>';
        listItem    +=  '<span class="price">$' + product.price + '</span>';
        listItem    +=  '<span class="description">' + product.description + '</span>';
        listItem    +=  '<a href="#" class="edit">Edit</a>';
        listItem    +=  '<a href="#" class="remove">Remove</a>';
        listItem    +=  '</li>';
                                
        catalogList.innerHTML += listItem;

        // add eventListener to the last product added
        // latter on need to create edit eventListener. 
        const lastProduct = catalogList.lastChild;
        const lastProductRemoveButton = lastProduct.getElementsByClassName('remove')[0];

        lastProductRemoveButton.addEventListener('click', () => {
            productManager.removeProduct(lastProduct); 
        });  
    },
    removeProduct: (li) => { 
        catalogList.removeChild(li); 
    }
}; 

//just for having something on the screen 
let product = new Product('https://cdn.shopify.com/s/files/1/0215/7672/products/13217_f28298f1-6ed5-466c-9893-604edce3c33d0_1024x1024.jpg?v=1560245059', 'Doll Toy Story', '12.00', 'Speciel eddition doll from the movie Toy Story');
productManager.addProudct(product);
product = new Product("https://www.eatingtools.com/1327-product_page_default_retina/one-star-leather-tistix-chopsticks-case.jpg", 'Chopsticks', "4.49", 'Very quallity Chinesse chopsticks, no coronavirus promised');
productManager.addProudct(product);

/*******     Event Listeners    *******/

// add new product to catalog eventlistener
formElements.submitNewProduct.addEventListener('click', () => {

    // check if input is valid
    if(formElements.productName.value.length > 3 && parseInt(formElements.productPrice.value) != NaN){
        let product = new Product(
            formElements.imgSource.value, 
            formElements.productName.value, 
            formElements.productPrice.value,
            formElements.productDescription.value);
        productManager.addProudct(product);
    }
    else{
        alert('Invalid Input');
    }
});



//////////////////////////////////
const  modal = document.getElementById('add-product-modal');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


